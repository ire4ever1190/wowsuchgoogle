import com.google.actions.api.ActionRequest
import com.google.actions.api.ActionResponse
import com.google.actions.api.DialogflowApp
import com.google.actions.api.ForIntent
import com.google.actions.api.response.ResponseBuilder
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.Headers
import io.ktor.request.receiveText
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.util.toMap
import net.dean.jraw.http.UserAgent
import net.dean.jraw.models.Submission
import java.lang.Exception
import net.dean.jraw.oauth.*
import net.dean.jraw.http.OkHttpNetworkAdapter
import net.dean.jraw.http.NetworkAdapter
import net.dean.jraw.models.SubredditSort
import com.google.api.services.actions_fulfillment.v2.model.BasicCard
import com.google.api.services.actions_fulfillment.v2.model.Image
import io.ktor.application.install
import io.ktor.features.CallLogging

fun main() {
    val app = ActionsApp()
    embeddedServer(Netty, 80){
        install(CallLogging)
        routing {
            get("/") {

                //TODO add basic site
                var responseText = "Yo"
                println(responseText)
                call.respondText(responseText, ContentType.Text.Html)
            }
            post("/") {

                val headers: Headers = call.request.headers
                val answer = app.handleRequest(call.receiveText(), headers.toMap())
                var response: String
                try {
                    response = answer.get()
                    call.respondText(response, contentType = ContentType.Application.Json)
                } catch (e: Exception) {
                    println("Invalid JSON: ${call.receiveText()}")
                    response = "{'error': 'yes'}"
                    call.respondText(response, contentType = ContentType.Application.Json)
                    throw e
                }

            }
        }
    }.start(wait = true)
}

class ActionsApp : DialogflowApp() {
    var reddit = RedditApp()
    var commands = reddit.commands
    fun makeResponse(post: Submission, request: ActionRequest): ResponseBuilder {
        var simpleResponses = arrayListOf(
                "WOW nice stuff by ${post.title}",
                "Ha Ha nice",
                "Clean up on aisle 4 cause I puked and shidded all over the place since this was so good",
                "Wow damn that is some juicy stuff")
        var response = getResponseBuilder(request)
        var needs = commands.get(request.getParameter("command").toString())
        if (needs?.title == true && needs.url == true){
            response.add(simpleResponses.random())
                    .add(BasicCard()
                            .setTitle(post.title)
                            .setSubtitle("posted by /u/${post.author}")
                            .setImage(Image()
                                    .setUrl(post.url)
                                    .setAccessibilityText("It is a nice meme")))
            return response
        }
        if (needs?.body == true && needs.title == true){
            response.add(BasicCard()
                    .setTitle(post.title)
                    .setFormattedText(post.selfText)

            )
            return response

        }
        if (needs?.url == true && needs.url == false) {
            response.add(Image().setUrl(post.url))
            return response
        }

        else {
            return response.add("invalid command")
        }
    }
    @ForIntent("Default Welcome Intent")
    fun welcome(request: ActionRequest): ActionResponse {
        var warning = "I use memes and jokes from Reddit that do usually contain mature content"
        var simpleResponses = arrayListOf(
                "Barko my dude. $warning",
                "Yo yo yo we got some lit fam memes here my bro. $warning",
                "Woof Woof ($warning)",
                "I know the warning is annoying but $warning")
        val responseBuilder = getResponseBuilder(request)
        responseBuilder.add(simpleResponses.random())
        return responseBuilder.build()
    }
    //TODO implement edit distance algorithym
    @ForIntent("gimme")
    fun gimme(request: ActionRequest): ActionResponse {
        var parameter: String = request.getParameter("command").toString()
        var post = reddit.get(commands.get(parameter)?.subreddit.toString()).random()
        return makeResponse(post, request).build()
    }
}


class RedditApp {
    var commands = mapOf("meme" to Needs("memes", title = true, url = true))
    val credsFile = this::class.java.getResource("/creds.txt").readText()
    val creds = credsFile.split(" ")
    var credentials = Credentials.script(creds[0], creds[1], creds[2], creds[3])
    val content = arrayListOf<Submission>()
    var userAgent = UserAgent("bot", "Wow Such Joke", "v0.1", "ire4ever1190")
    var adapter: NetworkAdapter = OkHttpNetworkAdapter(userAgent)
    var reddit = OAuthHelper.automatic(adapter, credentials)
    init {
        println("starting reddit")
        updateContent()
        println("finished")
    }

    fun updateContent() {
        for ((key, value) in commands)  {
            var subreddit: String = value.subreddit
            var paginator = reddit.subreddit(subreddit).posts().sorting(SubredditSort.HOT).limit(50).build()
            var page = paginator.next()
            page.forEach {
                content += listOf(it)
                println("added post: ${it.id}, size is now ${content.size}")
            }
        }
    }
    fun get(subreddit: String): ArrayList<Submission> {
        var tempContent = arrayListOf<Submission>()
        content.forEach {
            if (it.subreddit == subreddit) {
                tempContent.add(it)
            }
        }
        return tempContent
    }
}

data class Needs(val subreddit: String, val title: Boolean = false, val url: Boolean = false, val body: Boolean = false)


