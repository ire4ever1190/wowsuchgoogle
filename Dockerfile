FROM registry.gitlab.com/ire4ever1190/alpine-arm-qemu:latest
RUN apk add --update openjdk8-jre-base
COPY build/libs/WowSuchGoogle.jar server.jar
EXPOSE 80/tcp
CMD ["java", "-jar", "-Xms200M", "-Xmx200M", "server.jar"]
